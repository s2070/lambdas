package contract;

import model.Animal;

public interface CheckTrait {
    boolean test(Animal a);
}
