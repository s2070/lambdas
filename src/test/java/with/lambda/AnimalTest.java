package with.lambda;

import model.Animal;
import org.junit.jupiter.api.Test;
import contract.CheckTrait;

import java.util.ArrayList;
import java.util.List;

class AnimalTest {

    @Test
    void name() {
        List<Animal> animals = new ArrayList<>();
        animals.add(new Animal("fish", false, true));
        animals.add(new Animal("kangaroo", true, false));
        animals.add(new Animal("rabbit", true, false));
        animals.add(new Animal("turtle", false, true));

        //lambda
        //Deferred Execution: code is specified now but will run later.
        CheckTrait checker = a -> a.canHop();

        check(animals, checker, " can hop");

        CheckTrait checker2 = Animal::canSwim;
        check(animals, checker2, " can swim");
    }

    private void check(List<Animal> animals, CheckTrait checker, String s) {
        for (Animal animal : animals) {
            //the general check
            if (checker.test(animal)) {
                System.out.println(animal + s);
            }
        }
    }
}