package without.lambda;

import contract.CheckTrait;
import model.Animal;

import java.util.ArrayList;
import java.util.List;

class AnimalTest {

    @org.junit.jupiter.api.Test
    void getSpecies() {
        List <Animal> animals = new ArrayList<>();
        animals.add(new Animal("fish", false, true));
        animals.add(new Animal("kangaroo", true, false));
        animals.add(new Animal("rabbit", true, false));
        animals.add(new Animal("turtle", false, true));

        CheckTrait checker = new CheckIfHopper();
        for(Animal animal : animals) {
            //the general check
            if(checker.test(animal)) {
                System.out.println(animal + " ");
            }
            System.out.println();
        }
    }
}